from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
import io
from dotenv import load_dotenv
from collections import Counter
import pymongo
from bson import json_util, ObjectId
from database import db
import filters

#Global params
load_dotenv(os.path.join(os.path.dirname(__file__),".env"))
userstack_api = os.environ.get("userstack_api")

user_agent_api = Blueprint('user_agents', __name__)

######## Update User Agent DB ########
@user_agent_api.route('/user-agents/update',methods=['GET'])
def user_agent_update(keep_in_house=False,create=False):
    print("Getting user agents from analytics db...",flush=True)
    user_agents_found=list(db.analytics.distinct('userAgent'))
    print("Getting currently identified user agents...",flush=True)
    user_agent_db = list(db.userAgents.distinct('_id'))
    for ua in user_agents_found:
        if ua not in user_agent_db:
            userstack = requests.get("http://api.userstack.com/detect?access_key="+str(userstack_api)+"&ua=" + str(ua)).json()
            print(userstack,flush=True)
            userstack['_id'] = userstack['ua']
            db.userAgents.insert_one(userstack)
            print("Added " + str(ua),flush=True)

    if keep_in_house:
        return "All done processing " +str(len(user_agents_found))+" user agents."
    else:
        return jsonify("All done processing " +str(len(user_agents_found))+" user agents.")

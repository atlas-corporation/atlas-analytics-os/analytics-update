import os

from dotenv import load_dotenv
import pymongo

load_dotenv(os.path.join(os.path.dirname(__file__), ".env"))

mongo_user = os.environ.get("mongo_user")
mongo_pw = os.environ.get("mongo_pw")
mongo_host = os.environ.get("mongo_host")
dao_mongo_user = os.environ.get("dao_mongo_user")
dao_mongo_pw = os.environ.get("dao_mongo_pw")
dao_mongo_host = os.environ.get("dao_mongo_host")

# Mongo Connect
db_env = os.environ.get("db_env")

#Mongo Connect
client = pymongo.MongoClient("mongodb+srv://"+mongo_user+":"+mongo_pw+"@"+mongo_host)
db = client[db_env]

#DAO Mongo Connect
dao_client = pymongo.MongoClient("mongodb+srv://"+dao_mongo_user+":"+dao_mongo_pw+"@"+dao_mongo_host)
dao_db = dao_client.analytics

db_hyperfy = client["hyperfy-analytics-staging"]

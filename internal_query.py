from audioop import add
from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import pymongo
from bson import json_util, ObjectId
from bson.json_util import dumps
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
import numpy
from dotenv import load_dotenv
from collections import Counter
from werkzeug.routing import FloatConverter as BaseFloatConverter
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager
from database import db
import filters #filter functions
import map
import land
import dynamic_maps
import userAgent
import godMode

class FloatConverter(BaseFloatConverter):
    regex = r'-?\d+(\.\d+)?'

utc=pytz.UTC

print("Running analytics query server powered by Atlas Corp")
app = Flask(__name__)
CORS(app)
# before routes are registered
app.url_map.converters['float'] = FloatConverter

#register blueprints
app.register_blueprint(map.maps_api)
app.register_blueprint(dynamic_maps.dynamic_maps_api)
app.register_blueprint(land.land_api)
app.register_blueprint(userAgent.user_agent_api)
app.register_blueprint(godMode.god_mode_api)

# Setup the Flask-JWT-Extended extension
app.config["JWT_SECRET_KEY"] = ""  # Change this!
jwt = JWTManager(app)

port= 4200
host='0.0.0.0'

@app.route('/',methods=['GET','POST'])
def health():
    return jsonify("Analytics [internal] query back-end is up and running!")

def identifyUser(address,ip,guest=False):
    #no ETH Address
    if guest:
        user = list(db.users.find({'address':address,'ip':ip}))
        if len(user) > 0:
            print("User " + str(address) + " already exists in DB.",flush=True)
            pass
        else:
            try:
                location = requests.get("http://ip-api.com/json/" + str(ip)).json()
            except:
                location = None
                print("Can't find location for ip " + str(ip),flush=True)
            user_data = {
                "ip":ip,
                "location":location,
                "guest":True,
                "address":address
            }
            db.users.insert_one(user_data)
    #user has wallet addr
    else:        
        user = list(db.users.find({'address':address,'ip':ip}))
        if len(user) > 0:
            print("User " + str(address) + " already exists in DB.")
            pass
        else:
            try:
                location = requests.get("http://ip-api.com/json/" + str(ip)).json()
            except:
                location = None
                print("Can't find location for ip " + str(ip),flush=True)
            try:
                avatar = has_avatar(address,True)
            except:
                avatar = None
                print("Could not find DCL avatar for " + str(address))
            user_data = {
                "address":address,
                "ip":ip,
                "location":location,
                "avatar":avatar,
                "guest":False
            }
            db.users.insert_one(user_data)
    return "User added."

def has_avatar(address,keep_in_house=False):
    try:
        profile = requests.get("https://catalyst.atlascorp.io/lambdas/profiles/?id=" +str(address).lower()).json()
        avatar = {
            'avatar':{
                'name': profile[0]['avatars'][0]['name'],
                'face': profile[0]['avatars'][0]['avatar']['snapshots']['face256']
            }
        }
    except:
        avatar = {'hasAvatar':False}
    if keep_in_house:
        return avatar
    else:
        return jsonify(avatar)

@app.route('/refresh')
def update_users():
    users = list(db.users.find({},{"_id":1,"ip":1,"address":1,"guest":1,"location":1}))
    for u in users:
        location = None
        avatar = None
        if "location" not in u or u['location'] == None:
            try:
                location = requests.get("http://ip-api.com/json/" + str(u['ip'])).json()
                db.users.update({'_id':u['_id']},{'$set':{'location':location}})
            except:
                pass
        try:
            avatar = has_avatar(u['address'],True)
            if avatar != {'hasAvatar':False}:
                db.users.update({'_id':u['_id']},{'$set':{'avatar':avatar}})
        except:
            pass
        if "guest" not in u:
            db.users.update({'_id':u['_id']},{'$set':{'guest':False}})
        print(u,location,avatar,flush=True)
    return "Users updated."

@app.route('/rerun/<database>')
def re_run(database="analytics"):
    stopwatch = time.time()
    print("Querying unique addresses from mongo...",flush=True)
    res = list(db[database].aggregate([
        {
            '$group': {
                '_id': {
                    'ip': '$ip',
                    'player': '$player',
                    'auth':'$auth'
                }
            }
        },
        {
            '$project': {
                '_id': 0,
                'ip': "$_id.ip",
                'player': "$_id.player",
                'auth': "$_id.auth"
            }
        }
        ],allowDiskUse=True))
    try:
        print("Retrieved addresses! There's " + str(len(res)) + ' of them found in ' + str(time.time() - stopwatch) + ' seconds.')
    except:
        print("your length function failed.",flush=True)

    users = dict((v['auth'],v) for v in res).values()
    print(str(len(users)) + " users found.",flush=True)
    for u in users:
        print("Adding user " + u['auth'] + " to userDB.",flush=True)
        if 'player' not in u:
            identifyUser(u['auth'],u['ip'],True)
        else:
            identifyUser(u['player'],u['ip'],False)
    return "All done."

@app.route('/atlas-coverage')
@app.route('/atlas-coverage/<eventName>')
def atlas_coverage(eventName=None):
    if eventName:
        event_filter = [{"$match":{"eventName":eventName}}]
    else:
        event_filter = []
    parcels = list(db.analytics.aggregate(event_filter + [
        {"$project":{"_id":0,"parcel":1}},
        {"$group":{'_id':'$parcel'}}
    ]))
    parcels = [str(x['_id'][0]) + "," + str(x['_id'][1]) for x in parcels]
    image_bytes = map.atlas_map(parcels,20)
    return send_file(image_bytes,mimetype="image/png")   
 
#### GOD MODE - ORDINARILY WOULD NEED A JWT ####
@app.route('/god-mode/scene-access',methods=['GET','POST'])
def god_mode_scene_access():
    return godMode.scene_access()

@app.route('/god-mode/scene-records',methods=['GET','POST'])
def god_mode_scene_records():
    return godMode.scene_records()

@app.route('/god-mode/scene-init',methods=['GET','POST'])
def god_mode_scene_init():
    return godMode.scene_init()

@app.route('/god-mode-cache/<data_type>',methods=['GET','POST'])
def god_mode_cache(data_type=None):
    return godMode.god_mode_cache(data_type)

@app.route('/god-mode/num-scenes',methods=['GET','POST'])
def god_mode_num_scenes():
    return godMode.num_scenes()

@app.route('/god-mode/num-records',methods=['GET','POST'])
def god_mode_num_records():
    return godMode.num_records()

@app.route('/god-mode/parcel-coverage',methods=['GET','POST'])
def god_mode_parcel_coverage():
    return godMode.parcel_coverage()

@app.route('/god-mode/num-users',methods=['GET','POST'])
def god_mode_num_users():
    return godMode.num_users()

@app.route('/god-mode/recent-app-users',methods=['GET','POST'])
def god_mode_recent_app_users():
    return godMode.recent_app_users()

@app.route('/god-mode/daily-active-logins',methods=['GET','POST'])
def god_mode_app_dals():
    return godMode.app_dals()

@app.route('/god-mode/app-daus',methods=['GET','POST'])
def god_mode_app_daus():
	return godMode.app_daus()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port) 

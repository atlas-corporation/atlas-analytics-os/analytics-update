from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import pymongo
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
from dotenv import load_dotenv

land_api = Blueprint('land', __name__)

@land_api.route('/owned-land/<address>',methods=['GET'])
def owned_land(address,keep_in_house=False):
    graphQL_query = '''
    {
    estates(first: 1000,where:{owner:"'''+address.lower()+'''"}) {
        data{
            name
        }
        parcels{
            x
            y
        }
    }
    parcels(first: 1000,where:{owner:"'''+address.lower()+'''"}){
        x
        y
    }
    }
    '''
    url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
    res = requests.post(url,json={'query': graphQL_query})
    res = res.json()
    
    land = {
        "parcels":[ [int(x['x']),int(x['y'])] for x in res['data']['parcels']],
        "estates":[{'name':x['data']['name'],'parcels':[ [int(y['x']),int(y['y'])] for y in x['parcels']]} for x in res['data']['estates']]
    }
    if keep_in_house:
        return land
    else:
        return jsonify(land)

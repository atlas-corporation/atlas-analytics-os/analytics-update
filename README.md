# analytics-update

### A component of the Atlas Analytics platform

## About Atlas Analytics

Atlas Analytics is a platform for obtaining and reporting on analytics in the metaverse which rely on the Decentraland protocol. The platform was made open source as part of a [Decentraland grant](https://decentraland.org/governance/proposal/?id=fe85ab06-618d-4181-960d-fc32d5f0a7e1) and this repository constitues on of the microservices that comprises that platform.

The Atlas Analytics stack has the following components:

* `analytics-ingress` : Express application to receive analytics data from in-world. Contains filters and checks for users to apply if desired.

* `atlas-analytics-app`: The front-end, react application users log into to view reports on their analytics. 

*  `analytics-query` : The back-end of the analytics reporting application, providing queries into the database of collected data. Written in python.

* `analytics-express-auth` : A microservice for validating signatures due to functionality unsupported in python. Written in express.

* **YOU ARE HERE** `analytics-update` : More back-end code, specifically for large queries that require caching periodically. Written in python.

* `analytics-update-cron` : A node-red application governing the periodic triggering of the `analytics-update` caching microservice.

*  `mongo-change-stream-server` : An event handler, written in express, to handle keeping a current index of active scenes and last updates.

* `mongo-change-stream-server-worlds` : An event handler, written in express, to handle keeping a current index of active scenes and last updates for worlds scenes.

## About analytics-update

This is the python flask application used to query the database using mongo aggregation pipelines for heavier queries. These endpoints serve as a caching layer for the `atlas-analytics-app` react front end. This microservice contains a set of endpoints that calculate data and loads them to various caching database collections incuding maps, world-wide statistics, and database totals. 

This application can be triggered manually or automated when coupled with the `analytics-update-cron` microservice.

## Deploying this Microservice

This microservice can be deployed using the dockerfile present in the repository. Simply run using `docker build -t analytics-update . && docker run analytics-update`. It should probably be run in concert with the `analytics-update-cron` microservice as it is meant to work in concert with its cron job microservice. In that case use `docker-compose build && docker-compose up -d` to execute.
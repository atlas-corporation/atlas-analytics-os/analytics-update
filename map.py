from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
import io
from dotenv import load_dotenv
from collections import Counter

import filters

domain = "http://atlas-server:5000" #change this

maps_api = Blueprint('maps', __name__)

@maps_api.route('/v2/tiles',methods=['GET','POST'])
def tiles():
    if request.method == "POST":
        req = request.json
        query_string + "?"
        needs_ampersand = False
        if 'x1' in req:
            if needs_ampersand:query_string += "&"
            query_string += req['x1']
        if 'y1' in req:
            if needs_ampersand:query_string += "&"
            query_string += req['y1']
        if 'x2' in req:
            if needs_ampersand:query_string += "&"
            query_string += req['x2']
        if 'y2' in req:
            if needs_ampersand:query_string += "&"
            query_string += req['y2']
        if 'include' in req:
            if needs_ampersand:query_string += "&"
            query_string += req['include']
        if 'exclude' in req:
            if needs_ampersand:query_string += "&"
            query_string += req['exclude']            

    else:
        query_string = request.query_string

    result = requests.get(domain + '/v2/tiles' + query_string)

    return jsonify({'data':result})

@maps_api.route('/v1/map',methods=['GET','POST'])
def atlas_map(parcels_from_in_house=False,is_full_map=False):
    query_string = "?"
    needs_ampersand = False
    if request.method == "POST":
        req = request.json
        if 'width' in req:
            if needs_ampersand:query_string += "&"
            query_string += "width=" + req['width']
        if 'height' in req:
            if needs_ampersand:query_string += "&"
            query_string += "height=" + req['height']
        if 'size' in req:
            if needs_ampersand:query_string += "&"
            query_string += "size=" + req['size']
        if 'selected' in req:
            if needs_ampersand:query_string += "&"
            query_string += "selected="
            for i in req['selected']:
                query_string += str(i) + ";"
            #need center
            middle = math.floor(len(req['selected'])/2)
            query_string += "&center=" + str(req['selected'][middle])
        if 'center' in req:
            if needs_ampersand:query_string += "&"
            query_string += "center=" + req['center']                
        if 'on-sale' in req:
            if needs_ampersand:query_string += "&"
            query_string += "on-sale=" + req['on-sale']            

    else:
        query_string = request.query_string.decode()
    
    if parcels_from_in_house:
        query_string = "?"
        if needs_ampersand:query_string += "&"
        query_string += "selected="
        for i in parcels_from_in_house:
            query_string += str(i) + ";"
        #need center
        middle = math.floor(len(parcels_from_in_house)/2)
        if is_full_map:
            query_string += "&center=0,0"    
        else:
            query_string += "&center=" + str(parcels_from_in_house[middle])
        print(query_string,flush=True)
    if is_full_map:
        query_string += "&size=6&width=2000&height=2000"

    result = requests.get(domain + '/v1/map.png' + query_string)
    image_bytes = io.BytesIO(result.content)
    print(domain + '/v1/map.png' + query_string,flush=True)
    if parcels_from_in_house:
        return image_bytes
    else:
        return send_file(image_bytes,mimetype="image/png")    

@maps_api.route('/estate-parcels/<x>/<y>',methods=['GET'])
def estate_parcels(x,y,keep_in_house=False):
    graphQL_query = '''
    {
    parcels(where:{x:'''+x+''',y:'''+y+'''}){
        id
        x
        y
        estate{
        parcels{
            x
            y
        }
        }
        nft{
        owner {
            id
        }
        }
    }
    }
    '''
    url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
    res = requests.post(url,json={'query': graphQL_query})
    res = res.json()
    parcels = res['data']['parcels'][0]['estate']['parcels']
    if keep_in_house:
        return parcels
    else:
        return jsonify(parcels)

@maps_api.route('/scene-parcels/<x>/<y>')
def scene_parcels(x,y,keep_in_house=False):
    url = filters.get_server() + "/content/entities/scene?pointer="+str(x)+","+str(y)
    res = requests.get(url).json()
    try:
        parcels = {
            "parcels":res[0]['metadata']['scene']['parcels'],
            "name":res[0]['metadata']['display']['title'],
        }
    except:
        parcels = {}
    
    #add thumbnail
    try:
        thumbnail = [x for x in res[0]['content'] if x['file'] == "scene-thumbnail.png" ]
        parcels['scene-thumbnail'] = filters.get_server() + "/content/contents/" + thumbnail['hash']
    except:
        pass

    if keep_in_house:
        return parcels
    else:
        return jsonify(parcels)

@maps_api.route('/estate-map/<x>/<y>')
def estate_map(x,y,keep_in_house=False):
    parcels = estate_parcels(x,y,True)
    parcel_string = []
    for p in parcels:
        parcel_string += [str(p['x']) + "," + str(p['y'])]
    image_bytes = atlas_map(parcel_string)
    return send_file(image_bytes,mimetype="image/png")    

@maps_api.route('/scene-map/<x>/<y>')
def scene_map(x,y,keep_in_house=False):
    parcels = scene_parcels(x,y,True)
    image_bytes = atlas_map(parcels['parcels'])
    return send_file(image_bytes,mimetype="image/png")    


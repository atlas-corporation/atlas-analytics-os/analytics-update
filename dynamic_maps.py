from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
import io
from dotenv import load_dotenv
from collections import Counter
import pymongo
from bson import json_util, ObjectId
from database import db, dao_db
import filters

dynamic_maps_api = Blueprint('dynamic_maps', __name__)

######## DCL TOPOLOGY ########
@dynamic_maps_api.route('/maps/road-proximity',methods=['GET'])
@dynamic_maps_api.route('/maps/road-proximity/<create>',methods=['GET'])
def roads(keep_in_house=False,create=False):
    if create:
        print("Recalculating road-proximity...",flush=True)
        with open('static_map_data/road_proximity_orig.json', 'r') as f:
            data = json.load(f)        
        output = {
            "data":{}
        }
        for d in data:
            output['data'][str(d['x']) + ","+str(d['y'])] = {
                'x':d['x'],
                'y':d['y'],
                'v':d['distance']
            }
        distances = [d['distance'] for d in data]
        output['max'] = max(distances)
        output['min'] = min(distances)
        data = output
        with open('static_map_data/road-proximity.json', 'w') as f:
            json.dump(data, f)
    else:
        with open('static_map_data/road_proximity.json', 'r') as f:
            data = json.load(f)

    if keep_in_house:
        return data
    else:
        return jsonify(data)

@dynamic_maps_api.route('/maps/plaza-proximity',methods=['GET'])
@dynamic_maps_api.route('/maps/plaza-proximity/<create>',methods=['GET'])
def plazas(keep_in_house=False,create=False):
    if create:
        print("Recalculating plaza-proximity...",flush=True)
        with open('static_map_data/plaza_proximity_orig.json', 'r') as f:
            data = json.load(f)        
        output = {
            "data":{}
        }
        for d in data:
            output['data'][str(d['x']) + ","+str(d['y'])] = {
                'x':d['x'],
                'y':d['y'],
                'v':d['distance']
            }
        distances = [d['distance'] for d in data]
        output['max'] = max(distances)
        output['min'] = min(distances)
        data = output
        with open('static_map_data/plaza-proximity.json', 'w') as f:
            json.dump(data, f)
    else:
        with open('static_map_data/plaza_proximity.json', 'r') as f:
            data = json.load(f)

    if keep_in_house:
        return data
    else:
        return jsonify(data)

@dynamic_maps_api.route('/maps/district-proximity',methods=['GET'])
@dynamic_maps_api.route('/maps/district-proximity/<create>',methods=['GET'])
def districts(keep_in_house=False,create=False):
    if create:
        print("Recalculating district-proximity...",flush=True)
        with open('static_map_data/district_proximity_orig.json', 'r') as f:
            data = json.load(f)        
        output = {
            "data":{}
        }
        for d in data:
            output['data'][str(d['x']) + ","+str(d['y'])] = {
                'x':d['x'],
                'y':d['y'],
                'v':d['distance']
            }
        distances = [d['distance'] for d in data]
        output['max'] = max(distances)
        output['min'] = min(distances)
        data = output
        with open('static_map_data/district-proximity.json', 'w') as f:
            json.dump(data, f)
    else:
        with open('static_map_data/district_proximity.json', 'r') as f:
            data = json.load(f)

    if keep_in_house:
        return data
    else:
        return jsonify(data)        

######## User Traffic ########
@dynamic_maps_api.route('/maps/atlas-analytics-coverage',methods=['POST'])
def atlas_analytics_coverage(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None

    #if not has_access(request.headers['Authorization'].split(" ")[1],req):
    #    return {"msg":"User does not have access to scene"},401
    
    try:
        analytics = list(db.analytics.aggregate(filters.time_bands(req) + [ #fix
            { "$group": {
                "_id": "$parcel", 
                "unique-users": { "$addToSet": "$player" },
            }},
            { "$addFields": { "count": {"$size":"$unique-users"}}}
        ] + filters.include_users(req)))
            
    except:
        return {"msg":"DB call failed"},500
    output = {
        "data":{},
    }
    for a in analytics:
        output['data'][str(a['_id'][0]) + ","+str(a['_id'][1])] = {
            'x':a['_id'][0],
            'y':a['_id'][1],
            'v':a['count']
        }
        counts = [a['count'] for a in analytics]
        output['max'] = max(counts)
        output['min'] = min(counts)
    if keep_in_house:
        return output
    else:
        if req and 'cache' in req and req['cache']:
            output['map-type'] = 'atlas-coverage'
            output['timestamp'] = req['timestamp']
            db.mapsCache.insert_one(output)
            return jsonify("Atlas coverage processed for " + str(req['timestamp']))
        else:
            return jsonify(output)

@dynamic_maps_api.route('/maps/atlas-analytics-traffic',methods=['POST'])
def atlas_analytics_traffic(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None

    #if not has_access(request.headers['Authorization'].split(" ")[1],req):
    #    return {"msg":"User does not have access to scene"},401
    
    try:
        analytics = list(db.analytics.aggregate(filters.scene_filter(req) + filters.time_bands(req) + filters.parcel_filter(req) + filters.branch_filter(req) + filters.realm_filter(req) + [ #fix
            {"$project":{"parcel":1,"player":1}},
            { "$group": {
                "_id": "$parcel", 
                "unique-users": { "$addToSet": "$player" },
            }},
            { "$addFields": { "count": {"$size":"$unique-users"}}}
        ] + filters.include_users(req)))
            
    except:
        return {"msg":"DB call failed"},500
    output = {
        "data":{},
    }
    for a in analytics:
        output['data'][str(a['_id'][0]) + ","+str(a['_id'][1])] = {
            'x':a['_id'][0],
            'y':a['_id'][1],
            'v':a['count']
        }
        counts = [a['count'] for a in analytics]
        output['max'] = max(counts)
        output['min'] = min(counts)
    if keep_in_house:
        return output
    else:
        return jsonify(output)

@dynamic_maps_api.route('/maps/decentraland-traffic',methods=['POST'])
def decentraland_traffic(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None

    #if not has_access(request.headers['Authorization'].split(" ")[1],req):
    #    return {"msg":"User does not have access to scene"},401
    
    #try:
    analytics = list(dao_db.islands.aggregate(filters.time_bands(req) + [ #fix
        {"$unwind":"$islands"},
        {"$unwind":"$islands.peers"},
        { "$group": {
            "_id": "$islands.peers.parcel", 
            "unique-users": { "$addToSet": "$islands.peers.address" },
        }},
        { "$addFields": { "count": {"$size":"$unique-users"}}}
    ] + filters.include_users(req),allowDiskUse=True))
        
    #except:
    #    return {"msg":"DB call failed"},500
    output = {
        "data":{},
    }

    for a in analytics:
        if a['_id'] not in [None, []]:
            output['data'][str(a['_id'][0]) + ","+str(a['_id'][1])] = {
                'x':a['_id'][0],
                'y':a['_id'][1],
                'v':a['count']
            }
        counts = [a['count'] for a in analytics]
        output['max'] = max(counts)
        output['min'] = min(counts)

    if keep_in_house:
        return output
    else:
        if req and 'cache' in req and req['cache']:
            if 'map-type' in req and req['map-type']:
                output['map-type'] = req['map-type']
            else:
                output['map-type'] = 'dcl-traffic'
            output['timestamp'] = req['timestamp']
            db.mapsCache.insert_one(output)
            return jsonify("Decentraland traffic processed for " + str(req['timestamp']))
        else:
            return jsonify(output)

@dynamic_maps_api.route('/maps/dcl-footprint/<address>',methods=['POST'])
def dcl_footprint(address,keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None

    #if not has_access(request.headers['Authorization'].split(" ")[1],req):
    #    return {"msg":"User does not have access to scene"},401
    
    analytics = list(db.outsideIn.aggregate(filters.time_bands(req) + [ #fix
        {"$unwind":"$payload.islands"},
        {"$unwind":"$payload.islands.peers"},
        {"$match":{"payload.islands.peers.address":address.lower()}},
        { "$group": {
            "_id": "$payload.islands.peers.parcel", 
            "count": { "$sum": 1 },
        }},
    ] + filters.include_users(req),allowDiskUse=True))
        
    output = {
        "data":{},
    }

    for a in analytics:
        if a['_id'] not in [None, []]:
            output['data'][str(a['_id'][0]) + ","+str(a['_id'][1])] = {
                'x':a['_id'][0],
                'y':a['_id'][1],
                'v':a['count']
            }
        counts = [a['count'] for a in analytics]
        output['max'] = max(counts)
        output['min'] = min(counts)
    if keep_in_house:
        return output
    else:
        return jsonify(output)

@dynamic_maps_api.route('/maps/atlas-footprint/<address>',methods=['POST'])
def atlas_footprint(address,keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None

    #if not has_access(request.headers['Authorization'].split(" ")[1],req):
    #    return {"msg":"User does not have access to scene"},401
    
    try:
        analytics = list(db.analytics.aggregate(filters.time_bands(req) + [ #fix
            {"$match":{"player":address.lower()}},
            { "$group": {
                "_id": "$parcel", 
                "count": { "$sum": 1 },
            }},
        ] + filters.include_users(req)))
            
    except:
        return {"msg":"DB call failed"},500
    output = {
        "data":{},
    }
    for a in analytics:
        output['data'][str(a['_id'][0]) + ","+str(a['_id'][1])] = {
            'x':a['_id'][0],
            'y':a['_id'][1],
            'v':a['count']
        }
        counts = [a['count'] for a in analytics]
        output['max'] = max(counts)
        output['min'] = min(counts)
    if keep_in_house:
        return output
    else:
        return jsonify(output)

@dynamic_maps_api.route('/maps/atlas-domination',methods=['GET','POST'])
def atlas_domination(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None    

    analytics = list(db.analytics.aggregate(filters.time_bands(req) + [ #fix
        {"$project":{"parcel":1}},
        { "$group": {
            "_id": "$parcel"
        }},
    ]))

    output = {
        "data":{},
    }
    for a in analytics:
        output['data'][str(a['_id'][0]) + ","+str(a['_id'][1])] = {
            'x':a['_id'][0],
            'y':a['_id'][1],
            'v':1
        }
        output['max'] = 1
        output['min'] = 1
    if keep_in_house:
        return output
    else:
        if req and 'cache' in req and req['cache']:
            if 'map-type' in req and req['map-type']:
                output['map-type'] = req['map-type']
            else:
                output['map-type'] = 'atlas-domination'
            output['timestamp'] = req['timestamp']
            db.mapsCache.insert_one(output)
            return jsonify("Atlas coverage processed for " + str(req['timestamp']))
        else:
            return jsonify(output)

######### CATALYST SERVER MAPS ###############
@dynamic_maps_api.route('/maps/scene-updates',methods=['GET','POST'])
def scene_updates(keep_in_house=False):
    return jsonify(requests.get("http://134.122.126.3:54321/scene-deployments").json())

######### SUBGRAPH MAPS ###############
@dynamic_maps_api.route('/maps/estates',methods=['POST'])
def get_estate_list(keep_in_house=False):
    stopwatch = time.time()
    estates = []
    ### iterate through parcel sales, 3 rows at a time for 100 total queries
    for i in range(0,10000,1000):
        print(i,time.time()-stopwatch,flush=True)
        graphQL_query = '''{
        estates(first:1000,where:{tokenId_lte:'''+str(i+1000)+''',tokenId_gt: '''+str(i)+'''}) {
            id
            tokenId
            owner{
                id
            }
            parcels {
                x
                y
            }
        }
        }'''
        url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
        res = requests.post(url,json={'query': graphQL_query})
        estates += res.json()['data']['estates']
        if res.json()['data']['estates'] == []:
            break

    if keep_in_house:
        return estates
    else:
        output = {"data":{},'max':1,'min':0}
        '''
        for x in range(-150,151):
            for y in range(-150,151):
                output['data'][str(x) + "," + str(y)] = {
                    "x":x,"y":y,"v":0
                }
        '''
        for e in estates:
            for p in e['parcels']:
                output['data'][str(p['x']) + "," + str(p['y'])] = {
                    'x':int(p['x']),'y':int(p['y']),'v':1
                }
        return jsonify(output)

@dynamic_maps_api.route('/maps/air-rights',methods=['POST'])
def air_rights(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None
    output = {"data":{}}
    preprocessing_timer = time.time()
    for x in range(-150,151):
        for y in range(-150,151):
            output['data'][str(x) + "," + str(y)] = {
                "x":x,"y":y,"v":20
            }
    print("Preprocessing time:" ,time.time() - preprocessing_timer,flush=True)
    estates = get_estate_list(True)
    for e in estates:
        for p in e['parcels']:
            #crazy 151s and shit
            parcel_string = str(p['x']) + "," + str(p['y']) 
            if parcel_string in output['data']:
                output['data'][str(p['x']) + "," + str(p['y'])]['v'] = math.log2(len(e['parcels'])+1) * 20 
            else:
                output['data'][str(p['x']) + "," + str(p['y'])] = {
                    "x":int(p['x']),"y":int(p['y']),"v":math.log2(len(e['parcels'])+1) * 20 
                }
            
    if keep_in_house:
        return output
    else:
        if req and 'cache' in req and req['cache']:
            output['map-type'] = 'air-rights'
            output['timestamp'] = req['timestamp']
            db.mapsCache.insert_one(output)
            return jsonify("Air rights processed for " + str(req['timestamp']))
        else:
            return jsonify(output)

@dynamic_maps_api.route('/maps/parcel-prices',methods=['POST'])
def parcel_prices(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None
    
    output = {"data":{}}
    stopwatch = time.time()

    ### iterate through parcel sales, 3 rows at a time for 100 total queries
    for i in range(-151,164,3):
        print(i,time.time()-stopwatch,flush=True)
        graphQL_query = '''
        {
        nfts(first:1000,where:{ searchParcelX_in:['''+str(i)+''','''+str(i+1)+''','''+str(i+2)+'''] }) {
            parcel{
            x
            y
            }
            orders(first:1, where:{status:sold},orderBy:updatedAt, orderDirection:desc) {
                price
                updatedAt
                status
            }
        }
        }
        '''
        url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
        res = requests.post(url,json={'query': graphQL_query})
        res = res.json()['data']['nfts']
        for j in res:
            output['data'][str(j['parcel']['x']) + "," + str(j['parcel']['y'])] = {
                'x':j['parcel']['x'],
                'y':j['parcel']['y'],
                'v':0,
                't':0
                }
            try:
                output['data'][str(j['parcel']['x']) + "," + str(j['parcel']['y'])]['v'] = int(j['orders'][0]['price']) / 1000000000000000000
                output['data'][str(j['parcel']['x']) + "," + str(j['parcel']['y'])]['t'] = int(j['orders'][0]['updatedAt'])
            except:
                pass#print("No trade data : " + str(j['parcel']['x'])  + "," + str(j['parcel']['y']) + ".")
   
    #iterate through estates, average price per parcel
    #currently 2-3000, one query
    ### iterate through parcel sales, 3 rows at a time for 100 total queries
    for e in range(0,10000,1000):
        print(e,time.time()-stopwatch,flush=True)    
        graphQL_query ='''{
        orders(first: 1000, skip: '''+str(e)+''', 
            where: {category_in:[estate],status:sold}) {
            category
            price
            updatedAt
            nft {
            id
            name
            estate{
                parcels {
                x
                y
                }
            }
            }
        }
        }
        '''
        url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
        res = requests.post(url,json={'query': graphQL_query})
        res = res.json()['data']['orders']   
        #exit loop

        if len(res)==0:
            break
        for k in res:
            if len(k['nft']['estate']['parcels']) > 0:
                avgprice = int(k['price']) / len(k['nft']['estate']['parcels']) / 1000000000000000000
                for p in k['nft']['estate']['parcels']:
                    #TEMP
                    if str(p['x']) + "," + str(p['y']) not in output['data']:
                        output['data'][str(p['x']) + "," + str(p['y'])] = {
                            'x':p['x'],'y':p['y'],'v':0,'t':0
                        }
                    if avgprice > output['data'][str(p['x']) + "," + str(p['y'])]['v']:
                        output['data'][str(p['x']) + "," + str(p['y'])]['v'] = avgprice
                    if int(k['updatedAt']) > int(output['data'][str(p['x']) + "," + str(p['y'])]['t']):
                        output['data'][str(p['x']) + "," + str(p['y'])]['t'] = k['updatedAt']                    
    
    ## auction 1 data
    auction1_data = auction1(True)
    for a in auction1_data:
        if a['v'] != False:
            if output['data'][str(a['x']) + "," + str(a['y'])]['v'] == 0:
                output['data'][str(a['x']) + "," + str(a['y'])]['v'] = a['v'] /  1000000000000000000
                output['data'][str(a['x']) + "," + str(a['y'])]['t'] = 1545523200 #first auction facebook date
    
    values = [value['v'] for key,value in output['data'].items()]
    output['max'] = max(values)
    output['min'] = max(values)

    times = [value['t'] for key,value in output['data'].items()]
    output['max-t'] = max(times)
    output['min-t'] = max(times)

    if keep_in_house:
        return output
    else:
        if req and 'cache' in req and req['cache']:
            output['map-type'] = 'parcel-sales'
            output['timestamp'] = req['timestamp']
            db.mapsCache.insert_one(output)
            return jsonify("Parcel sales processed for " + str(req['timestamp']))
        else:
            return jsonify(output)

@dynamic_maps_api.route('/maps/whale-creep',methods=['POST'])
def whale_creep(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None
    
    output = {"data":{}}
    stopwatch = time.time()

    ### iterate through parcel sales, 3 rows at a time for 100 total queries
    owners = []
    for i in range(-151,164,3):
        print(i,time.time()-stopwatch,flush=True)
        graphQL_query = '''
        {
        nfts(first:1000,where:{ searchParcelX_in:['''+str(i)+''','''+str(i+1)+''','''+str(i+2)+'''] }) {
            parcel{
            x
            y
            }
            owner {
                id
            }
        }
        }
        '''
        url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
        res = requests.post(url,json={'query': graphQL_query})
        res = res.json()['data']['nfts']
        for j in res:
            if j['owner']['id'] != "0x959e104e1a4db6317fa58f8295f586e1a978c297": #estate contract
                owners.append({
                    "x":int(j['parcel']['x']),
                    "y":int(j['parcel']['y']),
                    "v":j['owner']['id']
                })

    print("whale estates",time.time()-stopwatch,flush=True)
    estates = get_estate_list(True)
    print(len(estates),flush=True)
    for e in estates:
        if e['owner']['id'] != '0x9a6ebe7e2a7722f8200d0ffb63a1f6406a0d7dce': #road agent
            for p in e['parcels']:
                owners.append({
                    "x":int(p['x']),
                    "y":int(p['y']),
                    "v":e['owner']['id']
                })

    #consolidate
    print("whale consolidation",time.time()-stopwatch,flush=True)
    whale_list = list(set([o['v'] for o in owners]))
    output = {"data":{}}
    for whale in whale_list:
        whale_property = [w for w in owners if w['v'] == whale]
        for w in whale_property:
            output['data'][str(w['x']) + "," + str(w['y'])] = {
                "x":w['x'],
                'y':w['y'],
                "v":len(whale_property)
            }
    values = [value['v'] for key,value in output['data'].items()]
    output['max'] = max(values)
    output['min'] = max(values)
    if keep_in_house:
        return output
    else:
        if req and 'cache' in req and req['cache']:
            output['map-type'] = 'whale-creep'
            output['timestamp'] = req['timestamp']
            db.mapsCache.insert_one(output)
            return jsonify("Whale Creep processed for " + str(req['timestamp']))
        else:
            return jsonify(output)

##### OG DATA MAPS #####
@dynamic_maps_api.route('/auction1')
def auction1(keep_in_house=False): 
    #static file workaround
    with open('auction1.json') as f:
        data = json.load(f)
    if keep_in_house:
        return data  
    else:
        return jsonify(data)

##### MAP CACHE #####
@dynamic_maps_api.route('/map-cache/<map_type>')
def maps_cache(map_type,keep_in_house=False):
    if map_type == 'parcel-sales-timing':
        data = list(db.mapsCache.find({'map-type':'parcel-sales'},{'_id':0}).sort('timestamp',-1).limit(1))[0]
        if 'max' in data:
            data['max'] = data['max-t']
            data['min'] = data['min-t']
        else:
            values = [value['v'] for key,value in data['data'].items()]
            data['max'] = max(values)
            data['min'] = min(values)        
            for key,value in data['data'].items():
                value['v'] = int(value['t'])
                del value['t']
    else:
         data = list(db.mapsCache.find({'map-type':map_type},{'_id':0}).sort('timestamp',-1).limit(1))[0]
    if 'max' not in data or data['max'] == data['min']:
        values = [value['v'] for key,value in data['data'].items()]
        data['max'] = max(values)
        data['min'] = min(values)
    if not keep_in_house:
        return data
    else:
        return jsonify(data)

from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import pymongo
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
import pymongo
from database import db


######### DATA ENHANCEMENT FUNCTIONS ###############
def parcels_in_scene(sceneName):
    last = list(db.analytics.find({'sceneName':sceneName,'eventName':'load-timer'}).sort('timestamp',-1).limit(1))[0]
    try:
        parcels = last['data']['sceneInitData']['parcels']
        parcels = [[int(x.split(',')[0]),int(x.split(',')[1])] for x in parcels]
    except:
        parcels = []
    return parcels

def scene_filter(req):
    if req:
        if 'scene' in req:
            scene_filter = [ {"$match":{"sceneName":req['scene']} }]
            if 'include-scene-bleed' in req:
                if req['include-scene-bleed']:
                    pass
                else:
                    #only parcels in scene - get from latest load-time filtered by timestamp
                    if 'parcels' in req:
                        scene_filter = [ {"$match":{"sceneName":req['scene'],"parcel":{"$in":req['parcels']}} }]
                    else:   
                        scene_filter = [ {"$match":{"sceneName":req['scene'],"parcel":{"$in":parcels_in_scene(req['scene'])}} }]
            else:              
                if 'parcels' in req:
                        scene_filter = [ {"$match":{"sceneName":req['scene'],"parcel":{"$in":req['parcels']}} }]
                else:   
                    scene_filter = [ {"$match":{"sceneName":req['scene'],"parcel":{"$in":parcels_in_scene(req['scene'])}} }]
        else:
            scene_filter = []
        
    else:
        scene_filter = []
    
    return scene_filter

def branch_filter(req):
    scene_filter = []
    if 'branch' in req:
        if req['branch'] and req['branch'] != "":
            scene_filter += [{"$match":{"sceneBranch":req['branch']}}]
    return scene_filter

def realm_filter(req):
    scene_filter = []
    if 'realm' in req:
        if req['realm'] and req['realm'] != "":
            scene_filter += [{"$match":{"realm":req['realm']}}]
    return scene_filter

def bin_size(req):
    if req:
        if 'binSize' in req:
            return int(req['binSize'])
        else:
            return 5
    else:
        return 5

def timezoneOffset(req):
    if req:
        if 'timezoneOffset' in req:
            tz = int(req['timezoneOffset']) / -60
            if tz < 10 and tz > 0:
                tz = "+0" + str(abs(int(tz)))
            elif tz > -10 and tz < 0:
                tz = "-0" + str(abs(int(tz)))
            return tz
        else:
            return "+00"
    else:
        return "+00"

#Assumes user inputs ISO date format (so they dont have to look up timestamps)
def time_bands(req):
    if req:
        if 'start' in req and 'end' in req:
            start = dateutil.parser.isoparse(req['start']).timestamp()*1000
            end = dateutil.parser.isoparse(req['end']).timestamp()*1000
            timeband = [ {"$match":{"$expr": {
                "$and": [
                    {
                    "$gte": ['$timestamp',start]
                    },
                    {
                    "$lte": ['$timestamp',end]
                    }
                ]
                }
            }}]
        elif 'start' in req:
            start = dateutil.parser.isoparse(req['start']).timestamp()*1000
            timeband = [ {"$match":{"$expr":
                {
                "$gte": ["$timestamp",start]
                }    
            }}]
        elif 'end' in req:
            end = dateutil.parser.isoparse(req['end']).timestamp()*1000
            timeband = [ {"$match":{"$expr":
                {
                "$lte": ["$timestamp",end]
                }    
            }}]
        else:
            timeband = []

        return timeband
    else:
        return []

def include_users(req):
    projection = [{'$project':{'unique-users':0,'households':0,'wallets':0,'hephaestus-users':0,'hela-users':0,'heimdallr-users':0,"baldr-users":0,"artemis-users":0,"loki-users":0,"dg-users":0,"odin-users":0,"unicorn-users":0,"marvel-users":0,"athena-users":0}}]
    if req:
        if 'include-users' in req:
            if req['include-users']:
                projection = []
    return projection

def parcel_filter(req,outsideIn=False):
    match = []
    if req:
        if 'include-scene-bleed' in req and not req['include-scene-bleed']:
            if outsideIn:
                if 'parcel' in req:
                    match = [{"$match":{"payload.islands.peers.parcel":req['parcel']}}]
                elif 'parcels' in req:
                    match = [{"$match":{"payload.islands.peers.parcel":{"$in":req['parcels']}}}]          
            else:
                if 'parcel' in req:
                    match = [{"$match":{"parcel":req['parcel']}}]
                elif 'parcels' in req:
                    match = [{"$match":{"parcel":{"$in":req['parcels']}}}]
    return match

def pad_zeroes(req,time_series):
    padded_time_series = time_series
    if req:
        if 'zero-padded' in req and req['zero-padded']:
            padded_time_series = []
            if 'start' in req:
                start = dateutil.parser.isoparse(req['start'])
            else:
                start = time_series[0]['_id']
            if 'end' in req:
                end = dateutil.parser.isoparse(req['end'])
            else:
                end = time_series[-1]['_id']

            for i in range(int(datetime.datetime.timestamp(start)),int(datetime.datetime.timestamp(end)),bin_size(req)):
                t = [x for x in time_series if int(datetime.datetime.timestamp(x['_id'])) == i]
                
                if len(t)>0:
                    padded_time_series.append(t[0])
                else:
                    padded_time_series.append({
                        "_id":datetime.datetime.fromtimestamp(i),
                        "count":0
                    })
    return padded_time_series

def pad_zeroes_realms(req,time_series):
    padded_time_series = time_series
    if req:
        if 'zero-padded' in req and req['zero-padded']:
            padded_time_series = []
            if 'start' in req:
                start = dateutil.parser.isoparse(req['start'])
            else:
                start = time_series[0]['_id']
            if 'end' in req:
                end = dateutil.parser.isoparse(req['end'])
            else:
                end = time_series[-1]['_id']

            realms = set().union(*(d.keys() for d in time_series))
            realms = [x for x in realms if x != '_id']
            print(realms,flush=True)
            for i in range(int(datetime.datetime.timestamp(start)),int(datetime.datetime.timestamp(end)),bin_size(req)):
                t = [x for x in time_series if int(datetime.datetime.timestamp(x['_id'])) == i]
                
                if len(t)>0:
                    for r in realms:
                        if r not in t[0]:
                            t[0][r] = 0
                    padded_time_series.append(t[0])
                else:
                    ts = {"_id":datetime.datetime.fromtimestamp(i)}
                    for r in realms:
                        ts[r] = 0
                    padded_time_series.append(ts)
    return padded_time_series

def get_server():
    return "catalyst.atlascorp.io"
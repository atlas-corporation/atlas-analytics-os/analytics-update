from itertools import count
import re
from flask import Flask, jsonify, render_template, json, Response, request, send_file, Blueprint
from flask_cors import CORS, cross_origin
import requests
import os
import time
import pytz
import csv
import datetime
import dateutil.parser
import math
import io
from dotenv import load_dotenv
from collections import Counter
import pymongo
from bson import json_util, ObjectId
from database import db
import filters

god_mode_api = Blueprint('godMode', __name__)

######## Scene Stuff ########
def scene_access(keep_in_house=False):
    #Get Tags and other display data
    
    if request.method == 'POST':
        req = request.json
    else:
        req = None    
    try:
        analytics = list(db.analytics.aggregate(filters.time_bands(req)  + [ 
            {"$match": {'eventName':'load-timer','data.sceneInitData.analyticsVersion':{"$in":['0.3','0.4','0.4.2','0.4.3','0.4.4','0.4.5','0.4.6']}}},
            {"$sort": {'timestamp':1,'sceneName':1}},
            {'$group':{
                '_id':{'sceneName':"$sceneName","parcels":"$data.sceneInitData.parcels"},
                'lastUpdate':{'$last':'$timestamp'},
                'tags':{'$last':'$data.sceneInitData.tags'},
            }}
        ]))
    except:
        return {"msg":"DB call failed"},500
    #Now get owners
    owner_lookup = [p['_id']['parcels'][0] for p in analytics if 'parcels' in p['_id']]
    
    graphQL_query='''
    {
        nfts(first:1000,where:{ searchText_in: ''' +str(owner_lookup).replace("'",'"') + '''}) {
            parcel{
                x
                y
                owner{
                    id
                }
                estate{
                    id
                    tokenId
                    owner{
                        id
           	        }
                }
            }
        }
    }'''
    print(graphQL_query,flush=True)
    url = "https://api.thegraph.com/subgraphs/name/decentraland/marketplace"
    res = requests.post(url,json={'query': graphQL_query})
    res = res.json()
    print(res,flush=True)
    for a in analytics:
        a['access']=[]
        if a['tags']:
            a['access'] = [z.split("atlas:")[-1].lower() for z in a['tags'] if 'atlas' in z]
        if 'parcels' in a:
            x,y = a['_id']['parcels'][0].split(",")
            print(x,y,flush=True)
            graph_data = [g for g in res['data']['nfts'] if g['parcel']['x'] == x if g['parcel']['y']==y]
            if len(graph_data) >0 :
                graph_data = graph_data[0]
                print(a['access'],flush=True)
                print(graph_data,flush=True)
                try:
                    a['access'].append(graph_data['parcel']['estate']['owner']['id'])
                except:
                    a['access'].append(graph_data['parcel']['owner']['id'])

    if keep_in_house:
        return ""
    else:
        return jsonify(analytics)

def scene_init(keep_in_house=False):
    #Get Tags and other display data
    if request.method == 'POST':
        req = request.json
    else:
        req = None    
    print(req,flush=True)
    try:
        analytics = list(db.analytics.aggregate([ 
            {"$match": {'eventName':'load-timer','data.sceneInitData.analyticsVersion':{"$in":['0.3','0.4','0.4.2','0.4.3','0.4.4','0.4.5','0.4.6']}}},
            {"$project":{"eventName":1,"data.sceneInitData.parcels":1,"sceneName":1,"timestamp":1,"_id":0}},            
            {"$sort": {'timestamp':1,'sceneName':1}},
            {'$group':{
                '_id':{'sceneName':"$sceneName","parcels":"$data.sceneInitData.parcels"},
                'firstUpdate':{'$first':'$timestamp'}
            }}
        ],allowDiskUse=True))
    except:
        return {"msg":"DB call failed"},500
    if req and 'cache' in req and req['cache']:
        db.godModeCache.insert_one({
            "timestamp":req['timestamp'],
            "data-type":"scene-init",
            "analytics":analytics
        })
    if keep_in_house:
        return ""
    else:
        return jsonify(analytics)

def scene_records(keep_in_house=False):
    #Get Tags and other display data
    if request.method == 'POST':
        req = request.json
    else:
        req = None    
    try:
        analytics = list(db.analytics.aggregate([ 
            #{"$project":{"sceneName":1}},            
            {'$group':{
                '_id':{'sceneName':"$sceneName"},
                "count": { "$sum": 1 }
            }},
            {"$sort":{"count":-1}}
        ],allowDiskUse=True))
    except:
        return {"msg":"DB call failed"},500
    if req and 'cache' in req and req['cache']:
        db.godModeCache.insert_one({
            "timestamp":req['timestamp'],
            "data-type":"scene-records",
            "analytics":analytics
        })  
        return ""
    else:
        return jsonify(analytics)

def num_scenes(keep_in_house=False):
    #Get Tags and other display data
    if request.method == 'POST':
        req = request.json
    else:
        req = None    
    try:
        analytics=list(db.analytics.distinct('sceneName'))
    except:
        return {"msg":"DB call failed"},500
   
    if keep_in_house:
        return len(analytics)
    else:
        return jsonify({"data":len(analytics)})

def num_records(keep_in_house=False):
    #Get Tags and other display data
    if request.method == 'POST':
        req = request.json
    else:
        req = None    
    try:
        analytics=db.analytics.count()
    except:
        return {"msg":"DB call failed"},500
   
    if keep_in_house:
        return analytics
    else:
        return jsonify({"data":analytics})

def parcel_coverage(keep_in_house=False):
    #Get Tags and other display data
    if request.method == 'POST':
        req = request.json
    else:
        req = None    
    try:
        analytics=list(db.analytics.aggregate([
            {"$group":{
                "_id":"$parcel",
            }}
        ]))
    except:
        return {"msg":"DB call failed"},500

    if req and 'cache' in req and req['cache']:
        db.godModeCache.insert_one({
            "timestamp":req['timestamp'],
            "data-type":"parcel-coverage",
            "analytics":{
                "num-parcels":len(analytics),
                "dcl-coverage":len(analytics)/92513,
                "total-parcels":92513
            }
        })   

    if keep_in_house:
        return analytics
    else:
        return jsonify({"data":{
            "num-parcels":len(analytics),
            "dcl-coverage":len(analytics)/92513,
            "total-parcels":92513}})

def num_users(keep_in_house=False):
    #Get Tags and other display data
    if request.method == 'POST':
        req = request.json
    else:
        req = None    
    try:
        analytics=db.users.distinct('address')
    except:
        return {"msg":"DB call failed"},500
   
    if keep_in_house:
        return len(analytics)
    else:
        return jsonify({"data":len(analytics)})

def recent_app_users(keep_in_house=False):
    #Get Tags and other display data
    if request.method == 'POST':
        req = request.json
    else:
        req = None 
    now = (time.time() * 1000) - 86400000
    print(now,flush=True)
    #try:
    analytics=list(db.nonces.find({"nonce":{"$gte":now}},{"_id":0,"nonce":1,"address":1}))
    #except:
    #    return {"msg":"DB call failed"},500
   
    if keep_in_house:
        return analytics
    else:
        return jsonify({"data":{
            "users":analytics,
            "count":len(analytics)}})

def app_dals(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None  
        
    try:
        analytics = list(db.nonces.aggregate([
            { "$group": {
                "_id": { "$dateTrunc": { "date": {"$toDate":"$nonce"}, "unit": "millisecond", "binSize": 86400000} },
                "count": { "$sum": 1 },
            }},
            { "$sort": { "_id": -1 } }
        ]))
        
    except:
        return {"msg":"DB call failed"},500
    return jsonify({'data':analytics})

def app_daus(keep_in_house=False):
    if request.method == "POST":
        req = request.json
    else:
        req = None  
        
    try:
        analytics = list(db.nonces.aggregate([
            { "$group": {
                "_id": { "$dateTrunc": { "date": {"$toDate":"$nonce"}, "unit": "millisecond", "binSize": 86400000} },
                "unique-users": { "$addToSet": "$address" },
            }},
            { "$addFields": { "count": {"$size":"$unique-users"}}},
            { "$sort": { "_id": -1 } },
        ] + filters.include_users(req)))

    except:
        return {"msg":"DB call failed"},500    
        
    if 'include-users' in req and req['include-users']:
        unique_users = {}
        for a in analytics:
            unique_names = []
            for u in a['unique-users']:
                if u not in unique_users:
                    profile = requests.get("https://catalyst.atlascorp.io/lambdas/profiles/?id=" + str(u)).json()
                    try:
                        name = profile[0]['avatars'][0]['name']
                        unique_users[u] = {"address":u,"name":name}
                    except:
                        unique_users[u] = {"address":u,"name":"???"}
                    
                    try:
                        profilepic = profile[0] ['avatars'][0]['avatar']['snapshots']['face256']
                        unique_users[u]["face"] = profilepic
                    except:
                        unique_users[u]["face"]=None
                    unique_names.append(unique_users[u])
                else:
                    unique_names.append(unique_users[u])
                
            a['unique-names'] = unique_names
            del a['unique-users']

    if req and 'cache' in req and req['cache']:
        db.godModeCache.insert_one({
            "timestamp":req['timestamp'],
            "data-type":"app-daus",
            "analytics":analytics
        })         
    
    return jsonify({'data':analytics,'unique-users':unique_users})

##### DATA CACHE #####
def god_mode_cache(data_type,keep_in_house=False):
    data = list(db.godModeCache.find({'data-type':data_type},{'_id':0}).sort('timestamp',-1).limit(1))[0]
    if not keep_in_house:
        return data
    else:
        return jsonify(data)
